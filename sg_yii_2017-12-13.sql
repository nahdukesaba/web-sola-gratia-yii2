# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.20)
# Database: sg_yii
# Generation Time: 2017-12-13 12:34:37 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table anggota
# ------------------------------------------------------------

DROP TABLE IF EXISTS `anggota`;

CREATE TABLE `anggota` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT '',
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') DEFAULT NULL,
  `jenis_suara` enum('Sopran','Alto','Tenor','Bass') DEFAULT NULL,
  `angkatan` int(11) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT '',
  `alamat_tetap` varchar(100) DEFAULT NULL,
  `alamat_domisili` varchar(100) DEFAULT NULL,
  `no_handphone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `whatsapp` varchar(100) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `line_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `anggota` WRITE;
/*!40000 ALTER TABLE `anggota` DISABLE KEYS */;

INSERT INTO `anggota` (`id`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `jenis_suara`, `angkatan`, `jabatan`, `alamat_tetap`, `alamat_domisili`, `no_handphone`, `email`, `whatsapp`, `pekerjaan`, `line_id`)
VALUES
	(1,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(2,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(3,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(4,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(5,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(6,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(7,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(8,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(9,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(10,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(11,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(12,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(13,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(14,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(15,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(16,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(17,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(18,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(19,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(20,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(21,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(22,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(23,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(24,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(25,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(26,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(27,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(28,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(29,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(30,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(31,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(32,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(33,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(34,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(35,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(36,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(37,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(38,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(39,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(40,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(41,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(42,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(43,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(44,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(45,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(46,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(47,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(48,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(49,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(50,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(51,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(52,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(53,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(54,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(55,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(56,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(57,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(58,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(59,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(60,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(61,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(62,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(63,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(64,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba'),
	(65,'aasd','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba');

/*!40000 ALTER TABLE `anggota` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `lokasi` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(10000) DEFAULT NULL,
  `gambar_1` varchar(100) DEFAULT NULL,
  `gambar_2` varchar(100) DEFAULT NULL,
  `gambar_3` varchar(100) DEFAULT NULL,
  `gambar_4` varchar(100) DEFAULT NULL,
  `gambar_5` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table migration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;

INSERT INTO `migration` (`version`, `apply_time`)
VALUES
	('m000000_000000_base',1512492237),
	('m130524_201442_init',1512492245);

/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pelatih
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pelatih`;

CREATE TABLE `pelatih` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `periode` varchar(100) DEFAULT NULL,
  `event` varchar(500) DEFAULT NULL,
  `deskripsi` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pembina
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pembina`;

CREATE TABLE `pembina` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `periode` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sejarah
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sejarah`;

CREATE TABLE `sejarah` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) DEFAULT NULL,
  `visi` varchar(100) DEFAULT NULL,
  `misi` varchar(1000) DEFAULT NULL,
  `isi` varchar(10000) DEFAULT NULL,
  `gambar_1` varchar(100) DEFAULT NULL,
  `gambar_2` varchar(100) DEFAULT NULL,
  `gambar_3` varchar(100) DEFAULT NULL,
  `gambar_4` varchar(100) DEFAULT NULL,
  `gambar_5` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sejarah` WRITE;
/*!40000 ALTER TABLE `sejarah` DISABLE KEYS */;

INSERT INTO `sejarah` (`id`, `judul`, `visi`, `misi`, `isi`, `gambar_1`, `gambar_2`, `gambar_3`, `gambar_4`, `gambar_5`)
VALUES
	(65536,'Sejarah Sola','visi','misi','isi','','','','','');

/*!40000 ALTER TABLE `sejarah` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'solagratia','sgsbynyMb-7T91zZrhtTkE_m2htavGWG','$2y$13$Gs9vk4KMtSQKcPeGPlypSetkru.DwZHqil9PA/ugCK9Y4UOUdaoG6',NULL,'solagratia@gmail.com',10,1512492363,1512492363);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
