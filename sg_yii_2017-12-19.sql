# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.20)
# Database: sg_yii
# Generation Time: 2017-12-19 13:42:11 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table anggota
# ------------------------------------------------------------

DROP TABLE IF EXISTS `anggota`;

CREATE TABLE `anggota` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT '',
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') DEFAULT NULL,
  `jenis_suara` enum('Sopran','Alto','Tenor','Bass') DEFAULT NULL,
  `angkatan` int(11) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `alamat_tetap` text,
  `alamat_domisili` text,
  `no_handphone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `whatsapp` varchar(100) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `line_id` varchar(100) DEFAULT NULL,
  `gambar` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `anggota` WRITE;
/*!40000 ALTER TABLE `anggota` DISABLE KEYS */;

INSERT INTO `anggota` (`id`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `jenis_suara`, `angkatan`, `jabatan`, `alamat_tetap`, `alamat_domisili`, `no_handphone`, `email`, `whatsapp`, `pekerjaan`, `line_id`, `gambar`)
VALUES
	(1,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(2,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(3,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(4,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(5,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(6,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(7,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(8,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(9,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(10,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(11,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(12,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(13,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(14,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(15,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(16,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(17,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(18,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(19,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(20,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(21,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(22,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2008,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(23,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(24,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(25,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(26,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2009,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(27,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(28,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(29,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(30,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(31,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(32,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(33,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(34,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(35,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(36,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(37,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2007,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(38,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(39,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(40,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(41,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(42,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(43,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(44,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(45,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(46,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(47,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2006,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(48,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(49,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(50,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(51,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(52,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(53,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(54,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(55,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(56,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2005,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(57,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(58,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(59,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(60,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(61,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(62,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(63,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(64,'Ephraim','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(65,'aasd','Medan','1993-10-20','Laki-laki','Bass',2004,'Ketua Umum','Medan','Jakarta','082168751612','nahdukesaba@gmail.com','nahdukesaba','Swasta','nahdukesaba',NULL),
	(66,'gdsdg','',NULL,'Perempuan','Sopran',NULL,'','','','','','','','',''),
	(67,'asdfafsa','',NULL,'Perempuan','Sopran',NULL,'','','','','','','','','members/no_image.png'),
	(68,'','',NULL,'Perempuan','Sopran',NULL,'','','','','','','','',''),
	(69,'fda','','2014-01-17','Perempuan','Sopran',NULL,'','','','','','','','','');

/*!40000 ALTER TABLE `anggota` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` text,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `lokasi` text,
  `deskripsi` text,
  `gambar_1` varchar(100) DEFAULT NULL,
  `gambar_2` varchar(100) DEFAULT NULL,
  `gambar_3` varchar(100) DEFAULT NULL,
  `gambar_4` varchar(100) DEFAULT NULL,
  `gambar_5` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;

INSERT INTO `event` (`id`, `nama`, `tanggal_mulai`, `tanggal_selesai`, `lokasi`, `deskripsi`, `gambar_1`, `gambar_2`, `gambar_3`, `gambar_4`, `gambar_5`)
VALUES
	(1,'tes',NULL,NULL,'','','','','','',''),
	(2,'berhasil',NULL,NULL,NULL,NULL,'events/test.jpg',NULL,NULL,NULL,NULL),
	(3,'asdf',NULL,NULL,'','','','','','',''),
	(4,'asdf',NULL,NULL,'','','','','','',''),
	(5,'','2014-01-03','2014-01-10','','','events/Screen Shot 2017-12-13 at 9.32.25 PM.png','','','',''),
	(6,'',NULL,NULL,'','','Screen Shot 2017-12-13 at 9.36.44 PM.png','','','',''),
	(7,'',NULL,NULL,'','','events/Screen Shot 2017-12-13 at 9.32.25 PM.png','','','',''),
	(8,'gak ada',NULL,NULL,'','','events/Screen Shot 2017-12-13 at 9.32.25 PM.png','','','',''),
	(9,'vobam mana',NULL,NULL,'','','','events/Screen Shot 2017-12-13 at 9.36.44 PM.png','','','');

/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;

INSERT INTO `migration` (`version`, `apply_time`)
VALUES
	('m000000_000000_base',1512492237),
	('m130524_201442_init',1512492245);

/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pelatih
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pelatih`;

CREATE TABLE `pelatih` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `periode` varchar(100) DEFAULT NULL,
  `event` varchar(500) DEFAULT NULL,
  `deskripsi` text,
  `gambar` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pelatih` WRITE;
/*!40000 ALTER TABLE `pelatih` DISABLE KEYS */;

INSERT INTO `pelatih` (`id`, `nama`, `tempat_lahir`, `tanggal_lahir`, `periode`, `event`, `deskripsi`, `gambar`)
VALUES
	(1,'Huger Saragih','-','1975-08-03','','','Memulai karirnya terhadap dunia paduan suara sejak tergabung dengan PS. Consolatio pada tahun 1995. Setelah lulus kuliah iapun mencoba meningkatkan pemahaman serta pengetahuan mengenai paduan suara dengan aktif mengikuti workshop dan seminar paduan suara yang dibawakan oleh pakar paduan suara baik dari dalam maupun luar negeri. Iapun aktif membina PS. Sola Gratia SMA Negeri 1 Medan sebagai konduktor dan bersama-sama berhasil meraih prestasi selama ±15 tahun, yaitu sejak tahun 1999-2014.','pelatihs/huger.jpg'),
	(2,'Hiras Lumbantoruan','-','1986-04-15',NULL,NULL,'Memulai kecintaannya terhadap paduan suara sejak bergabung dengan PS. Sola Gratia pada tahun 2000. Setelah menyelesaikan studinya, ia pun meningkatkan pengetahuan dan pemahamannya mengenai paduan suara dengan aktif berpartisipasi dalam seminar dan kompetisi. Iapun aktif membina PS. Sola Gratia sebagai konduktor pada tahun 2014-2015. Hingga sekarang beliau tetap aktif membina PS.Sola Gratia sebagai alumni.','pelatihs/hiras.jpg'),
	(3,'Ken Steven','Binjai','1993-09-10',NULL,NULL,'Iapun memulai studi musiknya dibawah bimbingan Daud Kosasih (Indonesia) dan juga meraih gelar Sarjana Musik Gerejawinya dari AILM (The Asia Institute for Liturge and Music, Filipina) dengan area konsentrasi choral composition/conducting dibawah bimbingan Dr. Fransisco F.Feliciano, Ralph Hoffman dan Prof. Eudenice Palaruan. Iapun telah menghasilkan banyak karya yang ditampilkan dalam berbagai festival dan kompetisi paduan suara baik nasional maupun Internasional. Beliau pun tergabung bersama PS. Sola Gratia SMA Negeri 1 Medan dan aktif sebagai konduktor sejak tahun 2015 hingga tahun 2017.','pelatihs/ken.jpg');

/*!40000 ALTER TABLE `pelatih` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pembina
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pembina`;

CREATE TABLE `pembina` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `periode` varchar(100) DEFAULT NULL,
  `deskripsi` text,
  `gambar` text,
  `alamat` text,
  `no_handphone` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pembina` WRITE;
/*!40000 ALTER TABLE `pembina` DISABLE KEYS */;

INSERT INTO `pembina` (`id`, `nama`, `tempat_lahir`, `tanggal_lahir`, `periode`, `deskripsi`, `gambar`, `alamat`, `no_handphone`)
VALUES
	(1,'Joster Manalu, S.Th','Padang Sidempuan','1961-01-14','','Beliau merupakan Guru Agama Kristen di SMA Negeri 1 Medan yang menjadi Pembina Paduan Suara Sola Gratia pada angkatan 6, angkatan 7, angkatan 8, angkatan 15 hingga saat ini sudah mencapai angkatan 22.','pembinas/pak-j.jpg','',''),
	(2,'Drs. Piner Lingga, M.Min','P.Manik','1966-05-28',NULL,'Beliau merupakan Guru Agama Kristen di SMA Negeri 1 Medan yang menjadi Pembina Paduan Suara Sola Gratia pada angkatan 6, angkatan 8, angkatan 9, angkatan 10,  angkatan 11, angkatan 12.','pembinas/pak-lingga.jpg','',''),
	(3,'R.Sihombing, S.Pd','P. Taput','1951-07-09',NULL,'Beliau merupakan Guru Bahasa Indonesia di SMA Negeri 1 Medan dan sudah menjadi Pembina Paduan Suara Sola Gratia pada angkatan 1, angkatan 2, angkatan 8, angkatan 11, angkatan 12, angkatan 13, angkatan 14.','pembinas/bu-hombing.jpg','',''),
	(4,'Diana D.Hutabarat, S.Pd','Sibolga','1955-08-05',NULL,'Menjadi Pembina Paduan Suara Sola Gratia pada angkatan 3, angkatan 4, angkatan 5.','pembinas/bu-diana.jpg','','');

/*!40000 ALTER TABLE `pembina` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sejarah
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sejarah`;

CREATE TABLE `sejarah` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `judul` text,
  `visi` text,
  `misi` text,
  `isi` text,
  `gambar_1` varchar(100) DEFAULT NULL,
  `gambar_2` varchar(100) DEFAULT NULL,
  `gambar_3` varchar(100) DEFAULT NULL,
  `gambar_4` varchar(100) DEFAULT NULL,
  `gambar_5` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sejarah` WRITE;
/*!40000 ALTER TABLE `sejarah` DISABLE KEYS */;

INSERT INTO `sejarah` (`id`, `judul`, `visi`, `misi`, `isi`, `gambar_1`, `gambar_2`, `gambar_3`, `gambar_4`, `gambar_5`)
VALUES
	(65536,'Sejarah Sola','Membawa musik klasik dalam perspektif baru yang dapat dinikmati dan menjangkau seluruh kalangan masyarakat, Paduan Suara Sola Gratia berkomitmen untuk tetap melayani, berprestasi, mendidik dan menghibur melalui musik.','1. Menjaga keberadaan Paduan Suara Sola Gratia dalam dunia musik khususnya paduan suara di Indonesia <br/> 2. Menjawab tantangan dalam dunia musik khususnya paduan suara untuk tetap berkarya sebagai tanggung jawab terhadap kemampuan dan prestasi yang telah diraih <br> 3. Menjadi fasilitator bagi masyarakat melalui musik paduan suara yang berkembang melalui konser dan kompetisi paduan suara <br> 4. Melayani Tuhan melalui wadah paduan suara baik di lingkungan gereja maupun sekolah','Terbentuknya Paduan Suara Sola Gratia berawal dari pelayanan siswa siswi Kristen dalam wadah PA. Bukit Sion SMA Negeri 1 Medan, yang setiap tahunnya menyelenggarakan Festival Koor dan Vokal Group (FKVG) se-Sumatera Utara. Sebagai penyelenggara FKVG se-Sumatera Utara, terbersit kerinduan untuk membentuk suatu paduan suara yang berlatih secara rutin dan mengisi pelayanan dalam kebaktian PA.Bukit Sion (PABS). Menindaklanjuti kerinduan tersebut, Kakak Boydo Panjaitan selaku Ketua PA Bukit Sion dan juga sebagai Ketua Seksi IB OSIS bersama-sama dengan pengurus lain yaitu Kakak Lukas Sormin (Ketua I OSIS), Kakak Chandra Situmeang (Sekretaris PABS), Kakak Sintyche Marpaung (Bendahara PABS), dan Kakak Erichson H.Tambunan (Sekretaris IB OSIS), kemudian mempersiapkan rencana pembentukan suatu organisasi Paduan Suara. Setelah melalui beberapa kali pertemuan, disepakatilah agar Paduan Suara yang dibentuk menjadi suatu kegiatan ekstrakurikuler yang secara organisasi berada di bawah Seksi IB OSIS SMA Negeri 1 Medan, dan secara koordinasi berada di bawah binaan PA Bukit Sion. <br> <br> \r\nPada awalnya nama yang dipilih adalah “Kyrie Eleison” dengan makna “Tuhan Kasihanilah”, mengingat beratnya pergumulan yang dialami oleh Kakak Boydo Panjaitan dkk untuk mendirikan Paduan Suara pada saat itu. Namun, setelah mendengar masukan dari Bapak J.Manalu, STh selaku Pembina PABS dan juga berkonsultasi dengan para kakak-kakak alumni PABS ditetapkanlah nama “Sola Gratia”. Nama “Sola Gratia” sendiri dipilih karena memiliki makna “Hanya karena anugrah Tuhan, kita diselamatkan”.  Disamping itu, nama “Sola Gratia” juga sudah pernah digunakan oleh kakak-kakak alumni PA Bukit Sion dalam rekaman album kaset Vokal Group pada tahun 1993-1994. <br><br>\r\nDan Puji Tuhan, pada tanggal 14 Maret 1997 hanya karena oleh anugrah Tuhan Yesus, Proposal pembentukan Paduan Suara Sola Gratia disetujui dan ditandatangani oleh Bapak Drs. Bahiman Rambe (Kepala Sekolah SMAN 1), Bapak. J. Manalu, STh (Pembina PA Bukit Sion), Kakak Suhendra Cipta (Ketua OSIS 1997), dan Kakak Boydo Panjaitan (Ketua PA Bukit Sion/Ketua Seksi  IB OSIS  1997),  dan sejak itu pula Paduan  Suara Sola Gratia resmi sebagai Ekskul baru di SMA Negeri 1 Medan. <br><br>\r\nDalam perkembangannya sampai saat ini, Paduan Suara Sola Gratia yang telah mencapai usia 20 tahun dan melahirkan 22 angkatan, telah banyak mengikuti dan memenangkan kompetisi paduan suara yang bertaraf nasional dan internasional dan juga telah menyelenggarakan beberapa konser sebagai wujud pelayanan memuji nama Tuhan Yesus. Semangat ber”Mazmur” dari seluruh anggota Paduan Suara Sola Gratia yang mengalir dari angkatan 1 sampai angkatan 22, inilah yang membuat untaian nada dan harmonisasi vokal tetap berkumandang sampai saat ini. Rasa persaudaraan sebagai pengikut Kristus yang memiliki talenta suara merdu dan indah akan meneguhkan Paduan Suara Sola Gratia sebagai wadah pelayanan bagi kita semua dalam memuliakan nama Tuhan. Kiranya, Paduan Suara Sola Gratia SMA Negeri 1 Medan bisa tetap mempertahankan eksistensinya sebagai salah satu paduan suara terbaik tingkat SMA yang dimiliki Indonesia dan semakin harum namanya sampai ke level Internasional. AMIN <br> <br>\r\n','histories/test.jpg','','','','');

/*!40000 ALTER TABLE `sejarah` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'solagratia','sgsbynyMb-7T91zZrhtTkE_m2htavGWG','$2y$13$Gs9vk4KMtSQKcPeGPlypSetkru.DwZHqil9PA/ugCK9Y4UOUdaoG6',NULL,'solagratia@gmail.com',10,1512492363,1512492363);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
