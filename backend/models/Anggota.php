<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;

/**
 * This is the model class for table "anggota".
 *
 * @property int $id
 * @property string $nama
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $jenis_kelamin
 * @property string $jenis_suara
 * @property int $angkatan
 * @property string $jabatan
 * @property string $alamat_tetap
 * @property string $alamat_domisili
 * @property string $no_handphone
 * @property string $email
 * @property string $whatsapp
 * @property string $pekerjaan
 * @property string $line_id
 * @property string $gambar
 */
class Anggota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'anggota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_lahir'], 'safe'],
            [['jenis_kelamin', 'jenis_suara', 'alamat_tetap', 'alamat_domisili'], 'string'],
            [['angkatan'], 'integer'],
            [['nama', 'tempat_lahir', 'jabatan', 'no_handphone', 'email', 'whatsapp', 'pekerjaan', 'line_id'], 'string', 'max' => 100],
            [['gambar'], 'file', 'skipOnEmpty' => true, 'extensions'=>'jpg, gif, png, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'jenis_suara' => 'Jenis Suara',
            'angkatan' => 'Angkatan',
            'jabatan' => 'Jabatan',
            'alamat_tetap' => 'Alamat Tetap',
            'alamat_domisili' => 'Alamat Domisili',
            'no_handphone' => 'No Handphone',
            'email' => 'Email',
            'whatsapp' => 'Whatsapp',
            'pekerjaan' => 'Pekerjaan',
            'line_id' => 'Line ID',
            'gambar' => 'Gambar',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(isset($this->gambar)){
            $this->gambar=UploadedFile::getInstance($this,'gambar');
            if(is_object($this->gambar)){
                $path=Yii::$app->basePath . '/web/images/members/';  //set directory path to save image
                $this->gambar->saveAs($path.$this->gambar);   //saving img in folder
                $this->gambar = 'members/'.$this->gambar;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('anggota', ['gambar' => $this->gambar], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }
    }
}
