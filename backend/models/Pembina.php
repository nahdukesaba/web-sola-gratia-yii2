<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;

/**
 * This is the model class for table "pembina".
 *
 * @property int $id
 * @property string $nama
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $periode
 * @property string $deskripsi
 * @property string $gambar
 * @property string $alamat
 * @property string $no_handphone
 */
class Pembina extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pembina';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_lahir'], 'safe'],
            [['nama', 'tempat_lahir', 'periode'], 'string', 'max' => 100],
            [['deskripsi', 'gambar', 'alamat', 'no_handphone'], 'string'],
            [['gambar'], 'file', 'skipOnEmpty' => true, 'extensions'=>'jpg, gif, png, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'periode' => 'Periode',
            'deskripsi' => 'Deskripsi',
            'gambar' => 'Gambar',
            'alamat' => 'Alamat',
            'no_handphone' => 'No Handphone',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(isset($this->gambar)){
            $this->gambar=UploadedFile::getInstance($this,'gambar');
            if(is_object($this->gambar)){
                $path=Yii::$app->basePath . '/web/images/pembinas/';  //set directory path to save image
                $this->gambar->saveAs($path.$this->gambar);   //saving img in folder
                $this->gambar = 'pembinas/'.$this->gambar;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('pembina', ['gambar' => $this->gambar], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }
    }
}
