<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;

/**
 * This is the model class for table "event".
 *
 * @property int $id
 * @property string $nama
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string $lokasi
 * @property string $deskripsi
 * @property string $gambar_1
 * @property string $gambar_2
 * @property string $gambar_3
 * @property string $gambar_4
 * @property string $gambar_5
 */
class Event extends \yii\db\ActiveRecord
{
    const IMAGE_PLACEHOLDER = '/no_image.png';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_mulai', 'tanggal_selesai'], 'safe'],
            [['nama', 'lokasi', 'deskripsi'], 'string'],
            [['gambar_1', 'gambar_2', 'gambar_3', 'gambar_4', 'gambar_5'], 'file', 'skipOnEmpty' => true, 
                'extensions'=>'jpg, gif, png, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tanggal_mulai' => 'Tanggal Mulai',
            'tanggal_selesai' => 'Tanggal Selesai',
            'lokasi' => 'Lokasi',
            'deskripsi' => 'Deskripsi',
            'gambar_1' => 'Gambar 1',
            'gambar_2' => 'Gambar 2',
            'gambar_3' => 'Gambar 3',
            'gambar_4' => 'Gambar 4',
            'gambar_5' => 'Gambar 5',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(isset($this->gambar_1)){
            $this->gambar_1=UploadedFile::getInstance($this,'gambar_1');
            if(is_object($this->gambar_1)){
                $path=Yii::$app->basePath . '/web/images/events/';  //set directory path to save image
                $this->gambar_1->saveAs($path.$this->gambar_1);   //saving img in folder
                $this->gambar_1 = 'events/'.$this->gambar_1;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('event', ['gambar_1' => $this->gambar_1], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }

        if(isset($this->gambar_2)){
            $this->gambar_2=UploadedFile::getInstance($this,'gambar_2');
            if(is_object($this->gambar_2)){
                $path=Yii::$app->basePath . '/web/images/events/';  //set directory path to save image
                $this->gambar_2->saveAs($path.$this->gambar_2);   //saving img in folder
                $this->gambar_2 = 'events/'.$this->gambar_2;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('event', ['gambar_2' => $this->gambar_2], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }

        if(isset($this->gambar_3)){
            $this->gambar_3=UploadedFile::getInstance($this,'gambar_3');
            if(is_object($this->gambar_3)){
                $path=Yii::$app->basePath . '/web/images/events/';  //set directory path to save image
                $this->gambar_3->saveAs($path.$this->gambar_3);   //saving img in folder
                $this->gambar_3 = 'events/'.$this->gambar_3;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('event', ['gambar_3' => $this->gambar_3], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }

        if(isset($this->gambar_4)){
            $this->gambar_4=UploadedFile::getInstance($this,'gambar_4');
            if(is_object($this->gambar_4)){
                $path=Yii::$app->basePath . '/web/images/events/';  //set directory path to save image
                $this->gambar_4->saveAs($path.$this->gambar_4);   //saving img in folder
                $this->gambar_4 = 'events/'.$this->gambar_4;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('event', ['gambar_4' => $this->gambar_4], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }

        if(isset($this->gambar_5)){
            $this->gambar_5=UploadedFile::getInstance($this,'gambar_5');
            if(is_object($this->gambar_5)){
                $path=Yii::$app->basePath . '/web/images/events/';  //set directory path to save image
                $this->gambar_5->saveAs($path.$this->gambar_5);   //saving img in folder
                $this->gambar_5 = 'events/'.$this->gambar_5;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('event', ['gambar_5' => $this->gambar_5], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }
    }

    public function getDisplayImage() {
        if (empty($model->image_file)) {
            // if you do not want a placeholder
            $image = null;
     
            // else if you want to display a placeholder
            $image = Html::img(self::IMAGE_PLACEHOLDER, [
                'alt'=>Yii::t('app', 'No image yet'),
                'title'=>Yii::t('app', 'Click remove button below to remove this image'),
                'class'=>'file-preview-image'
                // add a CSS class to make your image styling consistent
            ]);
        }
        else {
            $image = Html::img(Yii::$app->basePath . '/web/images/events/' . $model->gambar_1, [
                'alt'=>Yii::t('app', 'Image for ') . $model->nama,
                'title'=>Yii::t('app', 'Click remove button below to remove this image'),
                'class'=>'file-preview-image'
                // add a CSS class to make your image styling consistent
            ]);
        }
     
        // enclose in a container if you wish with appropriate styles
        return ($image == null) ? null : 
            Html::tag('div', $image, ['class' => 'file-preview-frame']); 
    }

    public function deleteImage() {
        $image = Yii::$app->basePath . '/web/images/events/' . $model->gambar_1;
        if (unlink($image)) {
            $this->image_file = null;
            $this->save();
            return true;
        }
        return false;
    }
}
