<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Sejarah;

/**
 * SejarahSearch represents the model behind the search form of `backend\models\Sejarah`.
 */
class SejarahSearch extends Sejarah
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['judul', 'visi', 'misi', 'isi', 'gambar_1', 'gambar_2', 'gambar_3', 'gambar_4', 'gambar_5'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sejarah::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'visi', $this->visi])
            ->andFilterWhere(['like', 'misi', $this->misi])
            ->andFilterWhere(['like', 'isi', $this->isi])
            ->andFilterWhere(['like', 'gambar_1', $this->gambar_1])
            ->andFilterWhere(['like', 'gambar_2', $this->gambar_2])
            ->andFilterWhere(['like', 'gambar_3', $this->gambar_3])
            ->andFilterWhere(['like', 'gambar_4', $this->gambar_4])
            ->andFilterWhere(['like', 'gambar_5', $this->gambar_5]);

        return $dataProvider;
    }
}
