<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;

/**
 * This is the model class for table "pelatih".
 *
 * @property int $id
 * @property string $nama
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $periode
 * @property string $event
 * @property string $deskripsi
 * @property string $gambar
 */
class Pelatih extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pelatih';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_lahir'], 'safe'],
            [['nama', 'tempat_lahir', 'periode'], 'string', 'max' => 100],
            [['event'], 'string', 'max' => 500],
            [['deskripsi'], 'string'],
            [['gambar'], 'file', 'skipOnEmpty' => true, 'extensions'=>'jpg, gif, png, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'periode' => 'Periode',
            'event' => 'Event',
            'deskripsi' => 'Deskripsi',
            'gambar' => 'Gambar',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(isset($this->gambar)){
            $this->gambar=UploadedFile::getInstance($this,'gambar');
            if(is_object($this->gambar)){
                $path=Yii::$app->basePath . '/web/images/pelatihs/';  //set directory path to save image
                $this->gambar->saveAs($path.$this->gambar);   //saving img in folder
                $this->gambar = 'pelatihs/'.$this->gambar;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('pelatih', ['gambar' => $this->gambar], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }
    }
}
