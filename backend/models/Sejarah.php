<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;

/**
 * This is the model class for table "sejarah".
 *
 * @property int $id
 * @property string $judul
 * @property string $visi
 * @property string $misi
 * @property string $isi
 * @property string $gambar_1
 * @property string $gambar_2
 * @property string $gambar_3
 * @property string $gambar_4
 * @property string $gambar_5
 */
class Sejarah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sejarah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'visi', 'misi', 'isi'], 'string'],
            [['gambar_1', 'gambar_2', 'gambar_3', 'gambar_4', 'gambar_5'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg,jpeg,png,gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'visi' => 'Visi',
            'misi' => 'Misi',
            'isi' => 'Isi',
            'gambar_1' => 'Gambar 1',
            'gambar_2' => 'Gambar 2',
            'gambar_3' => 'Gambar 3',
            'gambar_4' => 'Gambar 4',
            'gambar_5' => 'Gambar 5',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(isset($this->gambar_1)){
            $this->gambar_1=UploadedFile::getInstance($this,'gambar_1');
            if(is_object($this->gambar_1)){
                $path=Yii::$app->basePath . '/web/images/histories/';  //set directory path to save image
                $this->gambar_1->saveAs($path.$this->gambar_1);   //saving img in folder
                $this->gambar_1 = 'histories/'.$this->gambar_1;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('sejarah', ['gambar_1' => $this->gambar_1], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }

        if(isset($this->gambar_2)){
            $this->gambar_2=UploadedFile::getInstance($this,'gambar_2');
            if(is_object($this->gambar_2)){
                $path=Yii::$app->basePath . '/web/images/histories/';  //set directory path to save image
                $this->gambar_2->saveAs($path.$this->gambar_2);   //saving img in folder
                $this->gambar_2 = 'histories/'.$this->gambar_2;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('sejarah', ['gambar_2' => $this->gambar_2], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }

        if(isset($this->gambar_3)){
            $this->gambar_3=UploadedFile::getInstance($this,'gambar_3');
            if(is_object($this->gambar_3)){
                $path=Yii::$app->basePath . '/web/images/histories/';  //set directory path to save image
                $this->gambar_3->saveAs($path.$this->gambar_3);   //saving img in folder
                $this->gambar_3 = 'histories/'.$this->gambar_3;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('sejarah', ['gambar_3' => $this->gambar_3], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }

        if(isset($this->gambar_4)){
            $this->gambar_4=UploadedFile::getInstance($this,'gambar_4');
            if(is_object($this->gambar_4)){
                $path=Yii::$app->basePath . '/web/images/histories/';  //set directory path to save image
                $this->gambar_4->saveAs($path.$this->gambar_4);   //saving img in folder
                $this->gambar_4 = 'histories/'.$this->gambar_4;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('sejarah', ['gambar_4' => $this->gambar_4], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }

        if(isset($this->gambar_5)){
            $this->gambar_5=UploadedFile::getInstance($this,'gambar_5');
            if(is_object($this->gambar_5)){
                $path=Yii::$app->basePath . '/web/images/histories/';  //set directory path to save image
                $this->gambar_5->saveAs($path.$this->gambar_5);   //saving img in folder
                $this->gambar_5 = 'histories/'.$this->gambar_5;    //appending id to image name            
                \Yii::$app->db->createCommand()
                      ->update('sejarah', ['gambar_5' => $this->gambar_5], 'id = "'.$this->id.'"')
                      ->execute(); //manually update image name to db
            }
        }
    }
}
