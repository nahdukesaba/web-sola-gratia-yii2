<?php

namespace backend\controllers;

use Yii;
use backend\models\Sejarah;
use backend\models\SejarahSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SejarahController implements the CRUD actions for Sejarah model.
 */
class SejarahController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error', 'index'],//, 'view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['update'],//, 'delete', 'create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sejarah models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new SejarahSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
        // $model = $this->findModel(65536);

        return $this->render('view', [
            'model' => $this->findModel(65536),
        ]);
    }

    /**
     * Displays a single Sejarah model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    /**
     * Creates a new Sejarah model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Sejarah();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }

    //     return $this->render('create', [
    //         'model' => $model,
    //     ]);
    // }

    /**
     * Updates an existing Sejarah model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        // $model = $this->findModel(65536);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->render('view', [
        //         'model' => $this->findModel(65536),
        //     ]);
        // }

        // return $this->render('update', [
        //     'model' => $model,
        // ]);

        $model = $this->findModel(65536);
        $oldImage_1 = $model->gambar_1;
        $oldImage_2 = $model->gambar_2;
        $oldImage_3 = $model->gambar_3;
        $oldImage_4 = $model->gambar_4;
        $oldImage_5 = $model->gambar_5;
        if ($model->load(Yii::$app->request->post()))
        {
            $image_1 = UploadedFile::getInstance($model, 'gambar_1');
            $image_2 = UploadedFile::getInstance($model, 'gambar_2');
            $image_3 = UploadedFile::getInstance($model, 'gambar_3');
            $image_4 = UploadedFile::getInstance($model, 'gambar_4');
            $image_5 = UploadedFile::getInstance($model, 'gambar_5');
            if(!isset($image_1)){
                $model->gambar_1 = $oldImage_1;
            }
            if(!isset($image_2)){
                $model->gambar_2 = $oldImage_2;
            }
            if(!isset($image_3)){
                $model->gambar_3 = $oldImage_3;
            }
            if(!isset($image_4)){
                $model->gambar_4 = $oldImage_4;
            }
            if(!isset($image_5)){
                $model->gambar_5 = $oldImage_5;
            }
            if($model->save())
            {
                if(isset($image_1)){
                    $image_1->saveAs(Yii::$app->basePath . '/web/images/histories/' . $model->gambar_1);   
                }
                if(isset($image_2)){
                    $image_2->saveAs(Yii::$app->basePath . '/web/images/histories/' . $model->gambar_2);   
                }
                if(isset($image_3)){
                    $image_3->saveAs(Yii::$app->basePath . '/web/images/histories/' . $model->gambar_3);   
                }
                if(isset($image_4)){
                    $image_4->saveAs(Yii::$app->basePath . '/web/images/histories/' . $model->gambar_4);   
                }
                if(isset($image_5)){
                    $image_5->saveAs(Yii::$app->basePath . '/web/images/histories/' . $model->gambar_5);   
                }
            }
            return $this->render('view', [
                'model' => $this->findModel(65536),
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sejarah model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the Sejarah model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sejarah the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sejarah::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
