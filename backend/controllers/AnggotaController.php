<?php

namespace backend\controllers;

use Yii;
use backend\models\Anggota;
use backend\models\AnggotaSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\web\UploadedFile;
use yii\db\Query;

/**
 * AnggotaController implements the CRUD actions for Anggota model.
 */
class AnggotaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error', 'index', 'view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Anggota models.
     * @return mixed
     */
    public function actionIndex($angkatan=1)
    {
        // $query = Anggota::find()->where(['angkatan' => 2009]);
        // $countQuery = clone $query;
        // $pages = new Pagination(['totalCount' => $countQuery->count()]);
        // $models = $query->offset($pages->offset)
        //     ->limit($pages->limit)
        //     ->all();

        $myCondition = Yii::$app->user->isGuest;

        // $query = new Query;             
        // $query->select('angkatan')->from(TabAccount::anggota())->distinct();

        // $command = $query->createCommand();
        // $datas = $command->queryAll();

        // foreach ($datas as $data){

        // }

        $query = Yii::$app->db->CreateCommand('SELECT DISTINCT angkatan FROM anggota ORDER BY angkatan ASC')->queryAll();

        // $query = mysql_query("SELECT * FROM anggota WHERE angkatan=$angkatan");
        // while($row = mysql_fetch_array($query))
        // {

        // }

        $searchModel = new AnggotaSearch();
        $searchModel->angkatan = $angkatan;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'myCondition' => $myCondition,
            'query' => $query,
            // 'models' => $models,
            // 'pages' => $pages,
        ]);
    }

    /**
     * Displays a single Anggota model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Anggota model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Anggota();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Anggota model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        // $model = $this->findModel($id);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }

        // $model->gambar = Yii::$app->basePath . '/web/images/members/' . $model->gambar;
        // return $this->render('update', [
        //     'model' => $model,
        // ]);


        $model = $this->findModel($id);
        $oldImage = $model->gambar;
        if ($model->load(Yii::$app->request->post()))
        {
            $image = UploadedFile::getInstance($model, 'gambar');
            if(!isset($image)){
                $model->gambar = $oldImage;
            }
            if($model->save())
            {
                if(isset($image)){
                    $image->saveAs(Yii::$app->basePath . '/web/images/members/' . $model->gambar);   
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Anggota model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Anggota model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Anggota the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Anggota::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
