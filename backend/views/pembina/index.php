<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PembinaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pembina';
?>
<div class="container3">
    <div class="pembina-index">
    
        <div class="sejarah-img">
            <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/pembina.jpg', ['class'=>'imgSejarah']) ?>
        </div>

        <h1>Pembina Sola Gratia</h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
        <p>
            <?php if (Yii::$app->user->isGuest == false) {
                    echo Html::a('Create Pembina', ['create'], ['class' => 'btn btn-success']);
                }
            ?>
        </p>
        <div class="one">
            <div class="sejarah-isi">
            <?php 
                foreach ($dataProvider->models as $model){
                    echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $model->gambar, ['class'=>'foto-profil']);
                    echo Html::a($model->nama, ['pembina/view','id'=>$model->id], ['title' => 'View','class'=>'keterangan-profil']);
                    echo "<br><hr>";
                }
            ?>
            </div>
        </div>
        <div class="two">
                <?php
                    $rows = (new \yii\db\Query())
                    ->select(['id','nama', 'tanggal_mulai','tanggal_selesai','lokasi', 'deskripsi', 'gambar_1','gambar_2','gambar_3','gambar_4','gambar_5'])
                    ->from('event')
                    ->limit(3)
                    ->orderBy(['id'=>SORT_DESC])
                    ->all();
                    foreach ($rows as $row){
                        echo "<div class='grid-sidebar'>";
                        echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $row['gambar_1'], ['class'=>'foto-icon']);
                        echo Html::a($row['nama'], ['event/view','id'=>$row['id']], ['title' => 'View']);
                        echo "</div>";
                        
                    }
                ?>
        </div>
    </div>
</div>
