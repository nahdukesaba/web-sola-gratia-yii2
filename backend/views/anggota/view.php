<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Anggota */

$this->title = $model->id;
?>
<div class="container3">
    <div class="anggota-view">
        <div class="sejarah-img">
            <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/anggota.jpg', ['class'=>'imgSejarah']) ?>
        </div>

    
        <p>
            <?php if (Yii::$app->user->isGuest == false) {
                    echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                }
            ?>
            <?php if (Yii::$app->user->isGuest == false) {
                    echo Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                }
            ?>
        </p>
        <div class="one">
                    <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $model->gambar, ['class'=>'foto-profil-detail']) ?>

        <h1><?= Html::encode($model->nama) ?></h1>
            <div class="detailAnggota">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
                //    'nama',
                    'tempat_lahir',
                    'tanggal_lahir',
                    'jenis_kelamin',
                    'jenis_suara',
                    'angkatan',
                    'jabatan',
                    'alamat_tetap',
                    'alamat_domisili',
                    'no_handphone',
                    'email:email',
                    'whatsapp',
                    'pekerjaan',
                    'line_id',
                ],
            ]) ?>
            </div>
        </div>
        <div class="two">
                <?php
                        $rows = (new \yii\db\Query())
                        ->select(['id','nama', 'tanggal_mulai','tanggal_selesai','lokasi', 'deskripsi', 'gambar_1','gambar_2','gambar_3','gambar_4','gambar_5'])
                        ->from('event')
                        ->limit(3)
                        ->orderBy(['id'=>SORT_DESC])
                        ->all();
                        foreach ($rows as $row){
                            echo "<div class='grid-sidebar'>";
                            echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $row['gambar_1'], ['class'=>'foto-icon']);
                            echo Html::a($row['nama'], ['event/view','id'=>$row['id']], ['title' => 'View']);
                            echo "</div>";
                        }
                    ?>
        </div>
    </div>
</div>