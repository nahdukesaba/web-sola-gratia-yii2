<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $searchModel app\models\AnggotaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Anggota';
?>

<div class="container3">
    <div class="anggota-index">
        <div class="sejarah-img">
            <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/anggota.jpg', ['class'=>'imgSejarah']) ?>
        </div>
    
        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
        <p>
            <?php if (Yii::$app->user->isGuest == false) {
                    echo Html::a('Create Anggota', ['create'], ['class' => 'btn btn-success']);
                }
            ?>
        </p>
        <div class="one" >
            <div class="dataAnggota">
                <table class="table_negative table-striped" style="margin-bottom: 50px;">
                </table>
            </div>
            <?php
            $rows = (new \yii\db\Query())
                        ->select(['id','angkatan','nama', 'jenis_suara'])
                        ->from('anggota')
                        ->orderBy(['angkatan'=>SORT_ASC])
                        ->all();
            
            ?>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.min.css" type="text/css" >
            <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.min.js"></script>
            
            <script>
            var data = [];
            
            function dataAnggota(){
                <?php foreach($rows as $row) { ?>  
                        data.push({ colom1 : ' <?php echo Html::a($row['angkatan'], ['angkatan/view','id'=>$row['id']]) ?>', colom2 : '<?php echo Html::a($row['jenis_suara'], ['anggota/view','id'=>$row['id']]) ?>', colom3 : '<?php echo Html::a($row['nama'], ['anggota/view','id'=>$row['id']]) ?>' });
                
                <?php } ?>
                return data;
            }
            
            jQuery(function($){
                $('.table_negative').footable({
                    "paging": {
                        "enabled": true,
                        "size" : 10
                    },
                    "filtering": {
                        "enabled": true
                    },
                    "sorting": {
                        "enabled": true
                    },
                    "columns": [{"name": "colom1", "title" :"Angkatan"}, {"name": "colom2", "title" :"Jenis Suara"}, {"name": "colom3", "title" :"Nama"}],
                    "rows":   dataAnggota()
                              
                });
            });
        </script>
        </div>
        <div class="two">
            <?php
                    $rows = (new \yii\db\Query())
                    ->select(['id','nama', 'tanggal_mulai','tanggal_selesai','lokasi', 'deskripsi', 'gambar_1','gambar_2','gambar_3','gambar_4','gambar_5'])
                    ->from('event')
                    ->limit(3)
                    ->orderBy(['id'=>SORT_DESC])
                    ->all();
                    foreach ($rows as $row){
                        echo "<div class='grid-sidebar'>";
                        echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $row['gambar_1'], ['class'=>'foto-icon']);
                        echo Html::a($row['nama'], ['event/view','id'=>$row['id']], ['title' => 'View']);
                        echo "</div>";
                    }
                ?>
        </div>
        <?php /* ListView::widget([
            'options' => [
                'tag' => 'div',
            ],
            'dataProvider' => $dataProvider,
            'itemView' => function ($model, $key, $index, $widget) {
                return '<div>' . $model->angkatan . '</div>';
            },
            'itemOptions' => [
                'tag' => false,
            ],
            'summary' => '',
            'layout' => '{items} {pager}',
            
            'pager' => [
                // Use custom pager widget class
                'class' => backend\widgets\pager\Pager::className(),
                
                // Configurations for default LinkPager widget are still available
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last',
                'maxButtonCount' => 4,
                            // Options for <ul> wrapper of default pager buttons are still available
                'options' => [
                    'class' => 'pagination',
                    'style' => 'display:inline-block;float:left;margin:20px 10px 20px 0;width:auto;'
                ],
                // Style for page size select
                'sizeListHtmlOptions' => [
                    'class' => 'form-control',
                    'style' => 'display:inline-block;float:left;margin:20px 10px 20px 0;width:auto;'
                ],
                // Style for go to page input
                'goToPageHtmlOptions' => [
                    'class' => 'form-control',
                    'style' => 'display:inline-block;float:left;margin:20px 10px 20px 0;width:auto;'
                ],
            ],
    
        ]);
        */?>
    
        <?php 
            // foreach ($models as $model) {
            //     // display $model here
            //     echo $model->nama;
            // }
    
            // echo LinkPager::widget([
            //     'pagination' => $pages,
            // ]);
    
        ?>
    
    </div>
</div>
