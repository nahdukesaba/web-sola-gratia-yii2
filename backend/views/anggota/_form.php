<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Anggota */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="anggota-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'options'=>['enctype'=>'multipart/form-data'],
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-4',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal_lahir')->widget(DatePicker::className(),[
            'clientOptions' => [
                'defaultDate' => '2014-01-01',
                // 'yearRange' => 'c-50:c-20',
                'changeMonth'=> true,
                'changeYear'=> true,
            ],
            'options'=>['style'=>'width:250px;', 'class'=>'form-control'],
            'dateFormat' => 'yyyy-MM-dd',
        ]) ?>

    <?= $form->field($model, 'jenis_kelamin')->dropDownList([ 'Perempuan' => 'Perempuan', 'Laki-laki' => 'Laki-laki', ]) ?>

    <?= $form->field($model, 'jenis_suara')->dropDownList([ 'Sopran' => 'Sopran', 'Alto' => 'Alto', 'Tenor' => 'Tenor', 'Bass' => 'Bass', ]) ?>

    <?= $form->field($model, 'angkatan')->textInput() ?>

    <?= $form->field($model, 'jabatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat_tetap')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat_domisili')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_handphone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'whatsapp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pekerjaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'line_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gambar')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false],
        'pluginOptions' => [
            'showUpload' => false,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"/> ',
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
