<?php
// _list_item.php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<article class="item" data-key="<?= $model->id; ?>">
    <a href="<?= Url::to('view?id='. $model->id)?>">
        <div class="grid1">
        
            <?= Html::img(Yii::$app->request->BaseUrl.'/images/' . $model['gambar_1'], ['class'=>'foto-icon']); ?>
                                
            <center><?= HTML::encode($model->nama) ?></center>
        </div>
    </a>
</article>