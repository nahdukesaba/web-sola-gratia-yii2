<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'options'=>['enctype'=>'multipart/form-data'],
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal_mulai')->widget(DatePicker::className(),[
            'clientOptions' => [
                'defaultDate' => '2014-01-01',
                // 'yearRange' => 'c-50:c-20',
                'changeMonth'=> true,
                'changeYear'=> true,
            ],
            'options'=>['style'=>'width:250px;', 'class'=>'form-control'],
            'dateFormat' => 'yyyy-MM-dd',
        ]) ?>

    <?= $form->field($model, 'tanggal_selesai')->widget(DatePicker::className(),[
            'clientOptions' => [
                'defaultDate' => '2014-01-01',
                // 'yearRange' => 'c-50:c-20',
                'changeMonth'=> true,
                'changeYear'=> true,
            ],
            'options'=>['style'=>'width:250px;', 'class'=>'form-control'],
            'dateFormat' => 'yyyy-MM-dd',
        ]) ?>

    <?= $form->field($model, 'lokasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deskripsi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gambar_1')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false],
        'pluginOptions' => [
            'showUpload' => false,
            // 'previewFileType' => 'image',
            // 'allowedFileExtensions'=>['jpg','gif','png'],
            // 'initialPreview'=>[
            //     Html::img(Yii::$app->basePath . '/web/images/' . $model->gambar_1)
            // ],
            // 'overwriteInitial'=>true,
            // 'initialCaption'=> $model->gambar_1,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"/>',
        ]
    ]) ?>

    <?= $form->field($model, 'gambar_2')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false],
        'pluginOptions' => [
            'showUpload' => false,
            // 'initialPreview'=>[
                // Html::img(Yii::$app->basePath . '/web/images/' . $model->gambar_1)
            // ],
            // 'initialCaption'=> $model->gambar_2,
            // 'overwriteInitial'=>true,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"/> ',
        ]
    ]) ?>

    <?= $form->field($model, 'gambar_3')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false],
        'pluginOptions' => [
            'showUpload' => false,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"/> ',
        ]
    ]) ?>

    <?= $form->field($model, 'gambar_4')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false],
        'pluginOptions' => [
            'showUpload' => false,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"/> ',
        ]
    ]) ?>

    <?= $form->field($model, 'gambar_5')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false],
        'pluginOptions' => [
            'showUpload' => false,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"/> ',
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
