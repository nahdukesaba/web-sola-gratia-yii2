<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event';
?>

<div class="container3">
    <div class="event-index">
        <div class="sejarah-img">
            <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/event.jpg', ['class'=>'imgSejarah']) ?>
        </div>
    
        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
        <p>
            <?php if (Yii::$app->user->isGuest == false) {
                    echo Html::a('Create Event', ['create'], ['class' => 'btn btn-success']);
                }
            ?>
        </p>
        <br><br>
        <?= 
            ListView::widget([
                'dataProvider' => $dataProvider,
                'options' => [
                    'tag' => 'div',
                    'class' => 'list-wrapper',
                    'id' => 'list-wrapper',
                ],
                'layout' => "{pager}\n{summary}\n{items}",
                'itemView' => function ($model, $key, $index, $widget) {
                    return $this->render('_list_item',['model' => $model]);
            
                    // or just do some echo
                    // return $model->title . ' posted by ' . $model->author;
                },
                'itemOptions' => [
                    'tag' => false,
                ],
                'pager' => [
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>>',
                ],
            ]);
        ?>
        
    </div>
</div>
