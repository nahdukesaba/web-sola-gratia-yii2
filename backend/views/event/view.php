<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Event */

$this->title = $model->id;
?>
<style>
#myImg {
    width:auto;
    height: 100px;
    border-radius: 10px;
    cursor: pointer;
    transition: 0.3s;
    margin-left:5px;
}
#myImg2 {
    width:auto;
    height: 100px;
    border-radius: 10px;
    cursor: pointer;
    transition: 0.3s;
    margin-left:5px;
}

#myImg3 {
    width:auto;
    height: 100px;
    border-radius: 10px;
    cursor: pointer;
    transition: 0.3s;
    margin-left:5px;
}

#myImg4 {
    width:auto;
    height: 100px;
    border-radius: 10px;
    cursor: pointer;
    transition: 0.3s;
    margin-left:5px;
}

#myImg5 {
    width:auto;
    height: 100px;
    border-radius: 10px;
    cursor: pointer;
    transition: 0.3s;
    margin-left:5px;
}
#myImg:hover {opacity: 0.7;}
#myImg2:hover {opacity: 0.7;}
#myImg3:hover {opacity: 0.7;}
#myImg4:hover {opacity: 0.7;}
#myImg5:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}
.close2 {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close2:hover,
.close2:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}


.close3 {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close3:hover,
.close3:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

.close4 {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close4:hover,
.close4:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

.close5 {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close5:hover,
.close5:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>


<div class="container3">
    <div class="event-view">
        <div class="sejarah-img">
            <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/event.jpg', ['class'=>'imgSejarah']) ?>
        </div>
        <p>
            <?php if (Yii::$app->user->isGuest == false) {
                    echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                }
            ?>
            <?php if (Yii::$app->user->isGuest == false) {
                    echo Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                }
            ?>
        </p>
    
        <h1><?= Html::encode($model->nama) ?></h1>
        <div class="sejarah-isi">
            <p><b><?= $model->lokasi ?></b></p>
    
            <p><?= $model->tanggal_mulai ?> - <?= $model->tanggal_selesai ?></p>
            
            <?= $model->deskripsi?>
        </div>

        <br>
        <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $model->gambar_1, ['id'=>'myImg']) ?>
        <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $model->gambar_2, ['id'=>'myImg2']) ?>
        <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $model->gambar_3, ['id'=>'myImg3']) ?>
        <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $model->gambar_4, ['id'=>'myImg4']) ?>
        <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $model->gambar_5, ['id'=>'myImg5']) ?>
        <div class="modal" id="myModal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01"> </img>
          <div id="caption"></div>
        </div>
        <div class="modal" id="myModal2">
          <span class="close2">&times;</span>
          <img class="modal-content" id="img02"> </img>
          <div id="caption"></div>
        </div>
        
        <div class="modal" id="myModal3">
          <span class="close3">&times;</span>
          <img class="modal-content" id="img03"> </img>
          <div id="caption"></div>
        </div>
        
        <div class="modal" id="myModal4">
          <span class="close4">&times;</span>
          <img class="modal-content" id="img04"> </img>
          <div id="caption"></div>
        </div>
        
        <div class="modal" id="myModal5">
          <span class="close5">&times;</span>
          <img class="modal-content" id="img05"> </img>
          <div id="caption"></div>
        </div>
        <script>
        
            // Get the modal
            var modal = document.getElementById('myModal');
            
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }
            
            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];
            
            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
        <script>
        
            // Get the modal
            var modal = document.getElementById('myModal2');
            
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg2');
            var modalImg = document.getElementById("img02");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }
            
            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close2")[0];
            
            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
        <script>
        
            // Get the modal
            var modal = document.getElementById('myModal3');
            
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg3');
            var modalImg = document.getElementById("img03");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }
            
            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close3")[0];
            
            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
        <script>
        
            // Get the modal
            var modal = document.getElementById('myModal4');
            
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg4');
            var modalImg = document.getElementById("img04");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }
            
            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close4")[0];
            
            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
        <script>
        
            // Get the modal
            var modal = document.getElementById('myModal5');
            
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg5');
            var modalImg = document.getElementById("img05");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }
            
            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close5")[0];
            
            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
    </div>
</div>
