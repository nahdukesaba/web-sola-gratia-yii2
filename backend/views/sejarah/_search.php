<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SejarahSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sejarah-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'judul') ?>

    <?= $form->field($model, 'visi') ?>

    <?= $form->field($model, 'misi') ?>

    <?= $form->field($model, 'isi') ?>

    <?php // echo $form->field($model, 'gambar_1') ?>

    <?php // echo $form->field($model, 'gambar_2') ?>

    <?php // echo $form->field($model, 'gambar_3') ?>

    <?php // echo $form->field($model, 'gambar_4') ?>

    <?php // echo $form->field($model, 'gambar_5') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
