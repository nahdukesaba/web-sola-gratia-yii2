<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sejarah */

$this->title = $model->judul;
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="container3">
        <div class="sejarah-view">
            <div class="sejarah-img">
                <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $model->gambar_1, ['class'=>'imgSejarah']) ?>
            </div>
            <div class="one">
    
                <div class="sejarah-isi">
                    <h1>Sejarah Sola Gratia</h1>
                
                    <p>
                        <?php //Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <p>
                            <?php if (Yii::$app->user->isGuest == false) {
                
                                    echo Html::a('Update', ['update'], ['class' => 'btn btn-primary']);
                                }
                            ?>
                        </p>
                
                        <?php /* Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) */?>
                    </p>
                    <?php echo $model->isi; ?>
                </div>
                <div class="sejarah-visi">
                    <br>
                    <?php echo $model->visi; ?>
                    <br>    
                    <?php echo $model->misi; ?>
                </div>
            </div>
            <div class="two">
                <br><br><br>
                <?php
                    $rows = (new \yii\db\Query())
                    ->select(['id','nama', 'tanggal_mulai','tanggal_selesai','lokasi', 'deskripsi', 'gambar_1','gambar_2','gambar_3','gambar_4','gambar_5'])
                    ->from('event')
                    ->limit(3)
                    ->orderBy(['id'=>SORT_DESC])
                    ->all();
                    foreach ($rows as $row){
                        echo "<div class='grid-sidebar'>";
                        echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $row['gambar_1'], ['class'=>'foto-icon']);
                        echo Html::a($row['nama'], ['event/view','id'=>$row['id']], ['title' => 'View']);
                        echo "</div>";
                    }
                ?>
            </div>
        </div>
</div>