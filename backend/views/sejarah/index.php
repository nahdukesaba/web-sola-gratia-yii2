<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SejarahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sejarah';
?>
<div class="sejarah-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if (Yii::$app->user->isGuest == false) {
                echo Html::a('Create Sejarah', ['create'], ['class' => 'btn btn-success']);
            }
        ?>
    </p>

</div>
