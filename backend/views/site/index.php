<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
$this->title = 'My Yii Application';
?>
<div class="container3">
<iframe width="100%" height="610" src="https://www.youtube.com/embed/Yfe2osnXMnw" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe></div>
            <div class="col-lg-4" style="text-align:justify;text-justify:inter-word;">
                <h2>About Sola Gratia</h2>

                <p>Dalam perkembangannya sampai saat ini, Paduan Suara Sola Gratia yang telah mencapai usia 20 tahun dan melahirkan 22 angkatan, telah banyak mengikuti dan memenangkan kompetisi paduan suara yang bertaraf nasional dan internasional dan juga telah menyelenggarakan beberapa konser sebagai wujud pelayanan memuji nama Tuhan Yesus.
                <p><?php echo Html::a('See more &raquo;', ['sejarah/index'], ['class' => 'btn btn-default'])?></p>
            </div>
            <div class="col-lg-4" style="text-align:justify;text-justify:inter-word;">
                <h2>Event Sola Gratia</h2>

                <p>Paduan Suara Sola Gratia telah mengadakan 3 kali kunjungan daerah, 8 kali konser dan mengikuti berbagai kompetis baik dari tingkat daerah, nasional dan internasional. Paduan Suara Sola Gratia juga memenuhi undangan dari berbagai instasi dan golongan. Yang paling utama adalah Paduan Suara Sola Gratia melakukan kunjungan gereja secara berkala.</p>

                <p><?php echo Html::a('See more &raquo;', ['event/index'], ['class' => 'btn btn-default'])?></p>
            </div>
            <div class="col-lg-4" style="text-align:justify;text-justify:inter-word;">
                <h2>Anggota Sola Gratia</h2>

                <p>Hingga Saat ini Paduan Suara Sola Gratia Beranggotakan lebih dari 400 orang yang melingkupi siswa SMA dan alumni. Solidaritas dan kekeluargaan antar anggota masih terjalin baik hingga saat ini. Hal ini terbukti dengan dukungan dari alumnni dalam setiap kegiatan Paduan Suara Sola Gratia.</p>

                <p><?php echo Html::a('See more &raquo;', ['anggota/index'], ['class' => 'btn btn-default'])?></p>
            </div>
</div>