<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
?>
<div class="container3">
    <div class="site-contact">
        <div class="sejarah-img">
            <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/contact.png', ['class'=>'imgSejarah']) ?>
        </div>

        <div class="one">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
            </p>
            <p>
                SMA Negeri 1 Medan
            </p>
            <p>
                Jalan Teuku Cik Ditiro No.1, Madras Hulu, Medan Polonia, Madras Hulu, Medan Polonia, Kota Medan, Sumatera Utara 20152
            </p>
            <p>
                Telp. (061) 4511765
            </p>
            <p>
                HP. 082165100594
            </p>
            <p>
                Email. solagratia.choir@gmail.com
            </p>
            <br>
            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
        
                        <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
        
                        <?= $form->field($model, 'email') ?>
        
                        <?= $form->field($model, 'subject') ?>
        
                        <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
        
                        <?php /* $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                        ]) */?>
        
                        <div class="form-group">
                            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                        </div>
        
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <div class="two">
                <?php
                        $rows = (new \yii\db\Query())
                        ->select(['id','nama', 'tanggal_mulai','tanggal_selesai','lokasi', 'deskripsi', 'gambar_1','gambar_2','gambar_3','gambar_4','gambar_5'])
                        ->from('event')
                        ->limit(3)
                        ->orderBy(['id'=>SORT_DESC])
                        ->all();
                        foreach ($rows as $row){
                            echo "<div class='grid-sidebar'>";
                            echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $row['gambar_1'], ['class'=>'foto-icon']);
                            echo Html::a($row['nama'], ['event/view','id'=>$row['id']], ['title' => 'View']);
                            echo "</div>";
                        }
                    ?>
        </div>
    </div>
</div>