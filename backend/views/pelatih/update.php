<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pelatih */

$this->title = 'Update Pelatih';
$this->params['breadcrumbs'][] = ['label' => 'Pelatih', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pelatih-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
