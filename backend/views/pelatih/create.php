<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pelatih */

$this->title = 'Create Pelatih';
$this->params['breadcrumbs'][] = ['label' => 'Pelatih', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelatih-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
