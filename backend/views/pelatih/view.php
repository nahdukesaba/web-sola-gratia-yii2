<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pelatih */

$this->title = $model->id;
?>
<div class="container3">
    <div class="pelatih-view">
        <div class="sejarah-img">
            <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/pelatih.jpg', ['class'=>'imgSejarah']) ?>
        </div>
    
    
        <p>
            <?php if (Yii::$app->user->isGuest == false) {
                    echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                }
            ?>
            <?php if (Yii::$app->user->isGuest == false) {
                    echo Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                }
            ?>
        </p>
        <div class="one">
            <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $model->gambar, ['class'=>'foto-profil-detail']) ?>
    
            <h1><?= Html::encode($model->nama) ?></h1>
            <div class="sejarah-isi">
            </div>
            
            
            <table class="detail-profil">
              <tr>
              </tr>
              <tr>
                <td colspan="2"><div class="justify"><?php echo $model->deskripsi; ?></div><br></td>
              </tr>
              <tr>
                  <td> </td>
                  <td> </td>
              </tr>
              <tr>
                <td class="data"><b>Tempat/Tanggal Lahir</b></td>
                <td><?php echo $model->tempat_lahir . ', ' . $model->tanggal_lahir; ?></td>
              </tr>
              <tr>
                <td class="data"><b>Periode</b></td>
                <td><?php echo $model->periode; ?></td>
              </tr>
            </table>
        </div>
        <div class="two">
                <?php
                        $rows = (new \yii\db\Query())
                        ->select(['id','nama', 'tanggal_mulai','tanggal_selesai','lokasi', 'deskripsi', 'gambar_1','gambar_2','gambar_3','gambar_4','gambar_5'])
                        ->from('event')
                        ->limit(3)
                        ->orderBy(['id'=>SORT_DESC])
                        ->all();
                        foreach ($rows as $row){
                            echo "<div class='grid-sidebar'>";
                            echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $row['gambar_1'], ['class'=>'foto-icon']);
                            echo Html::a($row['nama'], ['event/view','id'=>$row['id']], ['title' => 'View']);
                            echo "</div>";
                        }
                    ?>
        </div>
    
    </div>
</div>