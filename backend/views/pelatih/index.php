<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PelatihSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pelatih';
?>
<div class="container3">
    <div class="pelatih-index">
         <div class="sejarah-img">
            <?php echo Html::img(Yii::$app->request->BaseUrl.'/images/pelatih.jpg', ['class'=>'imgSejarah']) ?>
        </div>
        
        <h1>Pelatih Sola Gratia</h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <div class="one">
            <p>
                <?php if (Yii::$app->user->isGuest == false) {
                        echo Html::a('Create Pelatih', ['create'], ['class' => 'btn btn-success']);
                    }
                ?>
            </p>
            <?php 
                foreach ($dataProvider->models as $model){
                    echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $model->gambar, ['class'=>'foto-profil']);
                    echo Html::a($model->nama, ['pelatih/view','id'=>$model->id], ['title' => 'View','class'=>'keterangan-profil']);
                    echo "<br><hr>";
                }
            ?>
        </div>
        <div class="two">
            <?php
                    $rows = (new \yii\db\Query())
                    ->select(['id','nama', 'tanggal_mulai','tanggal_selesai','lokasi', 'deskripsi', 'gambar_1','gambar_2','gambar_3','gambar_4','gambar_5'])
                    ->from('event')
                    ->limit(3)
                    ->orderBy(['id'=>SORT_DESC])
                    ->all();
                    foreach ($rows as $row){
                        echo "<div class='grid-sidebar'>";
                        echo Html::img(Yii::$app->request->BaseUrl.'/images/' . $row['gambar_1'], ['class'=>'foto-icon']);
                        echo Html::a($row['nama'], ['event/view','id'=>$row['id']], ['title' => 'View']);
                        echo "</div>";
                    }
                ?>
        </div>
    </div>
</div>