<?php

namespace backend\widgets\pager;

use Yii;
use yii\web\AssetBundle;

class PagerAsset extends AssetBundle
{
    public $js = [
        'js/pager.js'
    ];

    public $css = [
        /* You can add extra CSS file here if you need */
        // 'css/pager.css'
    ];

    public $depends = [
        // we will use jQuery
        'yii\web\JqueryAsset'
    ];

    public function init()
    {   
        // Base path of current widget
        $this->sourcePath = __DIR__ . "/assets";
        parent::init();
    }
}


?>